﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorFinal : MonoBehaviour {
	public bool triggered = false;
	public bool opened = false;

	public GameObject doorRight;
	public GameObject doorLeft;

	public Animator animator;
	public AudioSource audio;

	public AudioClip neon;

	public  GameObject[] lightsRoom;
	public  GameObject[] lightsCorridor;

	public Goal goal;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (!triggered && goal.finished) {
			triggered = true;
			OpenDoor ();
		}
	}


	void OpenDoor(){
		animator.Play ("DoorOpen");
		audio.Play ();

		foreach (GameObject light in lightsRoom) {
			light.GetComponentInChildren<Light> ().enabled = false;
		}

		foreach (GameObject light in lightsCorridor) {
			StartCoroutine (TurnOnLight(light.GetComponentInChildren<Light>(), Random.Range(1f, 3f)));
		}

	}

	IEnumerator TurnOnLight (Light light, float time){
		yield return new WaitForSeconds (time);
		light.enabled = true;
		audio.PlayOneShot (neon);
	}
}