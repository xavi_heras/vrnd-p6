﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour {
	public float forceFactor = 10f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider col){
		if (col.gameObject.name == "Ball"){	
			col.gameObject.GetComponent<Rigidbody> ().AddForce ((col.transform.position - transform.position) * forceFactor * Time.smoothDeltaTime);
		}
	}
}
