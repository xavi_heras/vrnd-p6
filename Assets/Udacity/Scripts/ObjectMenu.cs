﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMenu : MonoBehaviour {
	public bool available = true;
	public int maxAvailable = 0;
	public int currentAvailable = 0;
	// Use this for initialization
	void Start () {
		InitObject ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void InitObject(){
		currentAvailable = maxAvailable;
	}
}
