﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {

	public GameObject destination;
	public bool waitExit = false; //flag to ignore teleportation

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider col){
		if (!waitExit) {
			if (col.gameObject.name == "Ball") {					
				destination.GetComponent<Portal> ().waitExit = true;
				col.gameObject.transform.position = destination.transform.position;
			}
		}
	}

	void OnTriggerExit(Collider col){
		if (col.gameObject.name == "Ball"){					
			waitExit = false;
		}
	}

}
