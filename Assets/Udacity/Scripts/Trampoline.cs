﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour {
	public float power = 5f;
	public Animator animator;
	public Animation bounceAnimation;
	private AudioSource audio;

	private Vector3 orig;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter (Collision col){		
		if (col.gameObject.name == "Ball") {
			Rigidbody r;
			r = col.gameObject.GetComponent<Rigidbody> ();
			r.velocity *= power;

			if (r.velocity.magnitude > power) {
				r.velocity = Vector3.ClampMagnitude (r.velocity, power);
			}


			//Debug.Log ("Bounce!");	
			if (!animator.GetCurrentAnimatorStateInfo (0).IsName ("TrampolineBounce")) {
				animator.Play ("TrampolineBounce");
			}
			audio.Play ();
		}
	}
		
}
