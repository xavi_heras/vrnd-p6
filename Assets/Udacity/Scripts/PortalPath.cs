﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalPath : MonoBehaviour {
	public GameObject portalA;
	public GameObject portalB;
	private LineRenderer path;
	// Use this for initialization
	void Start () {
		path = GetComponent<LineRenderer> ();
		path.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		path.SetPosition (0, portalA.transform.position);
		path.SetPosition (1, portalB.transform.position);
	}
}
