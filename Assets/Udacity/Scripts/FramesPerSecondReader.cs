﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using TMPro;
using UnityEngine.UI;

public class FramesPerSecondReader : MonoBehaviour {

	public VRTK_FramesPerSecondViewer viewer;
	public Text text;
	public TextMeshProUGUI textMesh;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		textMesh.text = text.text;
		textMesh.color = text.color;
	}
}


