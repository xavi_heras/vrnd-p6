﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fan : MonoBehaviour{
	private AudioSource audio;
	public AudioClip fanAudio;


	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		audio.clip = fanAudio;
		audio.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		
}
