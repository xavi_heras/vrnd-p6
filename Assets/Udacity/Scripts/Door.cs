﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {
	public bool triggered = false;
	public bool opened = false;

	public GameObject doorRight;
	public GameObject doorLeft;

	public Animator animator;
	public AudioSource audio;

	public AudioClip neon;

	public  GameObject[] lightsRoom;
	public  GameObject[] lightsCorridor;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col){
		
		if (col.CompareTag("Player")){	
			
			OpenDoor ();
		}
	}

	void OnTriggerExit(Collider col){

		if (col.CompareTag("Player")){	
			
			//CloseDoor ();
		}
	}


	void OpenDoor(){

		GetComponent<BoxCollider> ().enabled = false;

		animator.Play ("DoorOpen");
		audio.Play ();

		foreach (GameObject light in lightsCorridor) {
			light.GetComponentInChildren<Light> ().enabled = false;
		}

		foreach (GameObject light in lightsRoom) {
			StartCoroutine (TurnOnLight(light.GetComponentInChildren<Light>(), Random.Range(1f, 3f)));
		}

	}

	IEnumerator TurnOnLight (Light light, float time){
		yield return new WaitForSeconds (time);
		light.enabled = true;
		audio.PlayOneShot (neon);
	}
}
