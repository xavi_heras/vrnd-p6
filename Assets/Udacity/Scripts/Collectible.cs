﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		this.transform.Rotate (new Vector3 (Random.Range(0,360), Random.Range(0,360), Random.Range(0,360)));
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Rotate (new Vector3 (Random.value, Random.value, Random.value));
	}

	void OnTriggerStay(Collider col){
		if (col.gameObject.name == "Ball"){	
			if (!col.GetComponent<Ball> ().playing) //cheating! player is collecting manually
				col.GetComponent<Ball> ().valid = false;
			else {				
				col.GetComponent<Ball> ().CollectibleImpact ();
				gameObject.SetActive (false);
			}
		}
	}
}
