﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Ball : MonoBehaviour {
	public Vector3 initialPos;
	public bool playing = false; //flag to check if the ball was throwed
	public bool valid = true;  //to control cheat
	public Material normalMaterial;
	public Material cheatMaterial;
	public Renderer meshLightRender;
	public GameObject goal;

	private Coroutine lastRoutine = null;

	private AudioSource audio;
	public AudioClip failSound;
	public AudioClip shrinkSound;
	public AudioClip growSound;
	public AudioClip pickCollectibleSound;

	public bool reseting = false;

	private Animator animator;

	void Start (){
		audio = GetComponent<AudioSource> ();
		animator = GetComponent<Animator> ();
		//this.transform.localScale = Vector3.zero;
		initialPos = transform.position;

		goal = GameObject.Find ("Goal");
		SetNormalBall ();

		//Reset ();
	}

	public void SetNormalBall(){
		valid = true;
		SetNormalMaterial ();
	}
		
	public void SetCheatBall(){
		valid = false;
		SetCheatMaterial ();
	}

	void SetNormalMaterial(){
		var mats = meshLightRender.materials;
		mats [0] = normalMaterial;
		meshLightRender.materials = mats;

		//meshRender.materials [1] = normalMaterial;


	}

	void SetCheatMaterial(){
		var mats = meshLightRender.materials;
		mats [0] = cheatMaterial;
		meshLightRender.materials = mats;

		//meshRender.materials [1] = cheatMaterial;
	}

	void OnCollisionEnter (Collision col){
		if (col.gameObject.CompareTag("Ground")) {
			//Debug.Log ("ball touching ground. reset.");
			ForceBallReset ();		
		}
	}

	public void ForceBallReset(){
		if (!reseting) {
			reseting = true;
			//Debug.Log ("ForceBallReset");
			if (lastRoutine != null)
				StopCoroutine (lastRoutine);

			audio.PlayOneShot (failSound);

			DisableBallGravity ();
			lastRoutine = StartCoroutine (DelayedReset ());
		}
	}

	public void CollectibleImpact(){
		audio.PlayOneShot (pickCollectibleSound);
	}

	void DisableBallGravity(){
		Physics.gravity = Vector3.zero;
		GetComponent<Rigidbody> ().velocity = new Vector3 ();
		GetComponent<Rigidbody> ().angularVelocity = new Vector3 ();
		GetComponent<Rigidbody> ().velocity = Vector3.up * (Random.Range(1,3)/6f);
		GetComponent<Rigidbody> ().angularVelocity = Random.rotation.eulerAngles / 360f;
	}

	IEnumerator DelayedReset (){
		SetCheatBall ();
		yield return new WaitForSeconds (2f);
		//iTween.ScaleTo (this.gameObject, iTween.Hash ("scale", new Vector3 (0f, 0f, 0f), "time", .5f));
		animator.Play ("BallShrink");

		audio.PlayOneShot (shrinkSound);

		yield return new WaitForSeconds (.5f);
		Reset ();
	}

	public void Reset (){
		if (lastRoutine!=null) StopCoroutine (lastRoutine);
		Physics.gravity = new Vector3 (0f, -9.81f, 0f);
		this.transform.localScale = Vector3.zero;
		//reset current ball
		GetComponent<Rigidbody> ().velocity = new Vector3 ();
		GetComponent<Rigidbody> ().angularVelocity = new Vector3 ();
		transform.position = initialPos;
		transform.SetParent (null);
		playing = false;
		SetNormalBall ();

		//iTween.ScaleTo (this.gameObject, iTween.Hash ("scale", new Vector3 (1f, 1f, 1f), "time", .5f));

		animator.Play ("BallGrow");

		audio.PlayOneShot (growSound);
		reseting = false;
		//reset collectibles
		CollectiblesManager.ResetCollectibles ();

		//reset goal
		goal.GetComponent<Goal>().Reset();
	}

	public void BallWarp(){
		animator.Play ("BallWarp");
	}
}
