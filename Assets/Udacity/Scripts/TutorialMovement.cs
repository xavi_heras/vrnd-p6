﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMovement : MonoBehaviour {
	public GameObject player;
	private Vector3 playerPosition;
	// Use this for initialization
	void Start () {
		playerPosition = player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (player.transform.position != playerPosition) {
			Destroy (this.gameObject);
		}
	}
}
