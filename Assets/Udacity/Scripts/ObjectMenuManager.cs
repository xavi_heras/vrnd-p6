﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ObjectMenuManager : MonoBehaviour {

	public List<GameObject> objectList;
	public TextMeshProUGUI caption;
	public TextMeshProUGUI captionNumber;

	public int currentObject = 0;

	//Solution
	public GameObject solution;
	public GameObject solutionSolid;

	// Use this for initialization
	void Start () {		

		solution = GameObject.Find ("Solution");
		if (solution != null)
			solution.SetActive (false);

		solutionSolid = GameObject.Find ("SolutionSolid");
		if (solutionSolid != null)
			solutionSolid.SetActive (false);
		
		foreach (Transform child in transform) {
			if (child.CompareTag ("MenuObject")) {
				if (child.GetComponent<ObjectMenu> ().available) {
					child.GetComponent<ObjectMenu> ().InitObject ();
					objectList.Add (child.gameObject);
				}
				child.gameObject.SetActive (false);
			}
		}
		if (objectList.Count > 1) {
			Show ();

			for (int i = 0; i < objectList.Count; i++) {
				currentObject = i;
				MenuRight ();

			}

			currentObject = 0;
			ShowCurrentObject ();
		}

		Hide ();
	}

	public void Show (){
		gameObject.SetActive (true);
	}

	public void Hide (){
		gameObject.SetActive (false);
	}

	public void MenuLeft(){
		objectList [currentObject].SetActive (false);
		currentObject--;
		if (currentObject < 0) {
			currentObject = objectList.Count - 1;
		}
		ShowCurrentObject ();
	}

	public void MenuRight(){
		objectList [currentObject].SetActive (false);
		currentObject++;
		if (currentObject > objectList.Count - 1) {
			currentObject = 0;
		}
		ShowCurrentObject ();
	}

	void ShowCurrentObject(){		
		objectList [currentObject].SetActive (true);
		UpdateObjectInfo ();
	}

	void UpdateObjectInfo(){
		caption.text = objectList [currentObject].name;
		captionNumber.text = "available: " + objectList [currentObject].GetComponent<ObjectMenu> ().currentAvailable + 
							 " of " + objectList [currentObject].GetComponent<ObjectMenu> ().maxAvailable;
	}

	public void SpawnCurrentObject(){
		if (objectList [currentObject].GetComponent<ObjectMenu> ().available){
			if (objectList [currentObject].GetComponent<ObjectMenu> ().currentAvailable > 0){

				GameObject go =	Instantiate (Resources.Load ("Prefabs/" + objectList [currentObject].name, typeof(GameObject)) as GameObject, 
					               objectList [currentObject].transform.position, 
					               objectList [currentObject].transform.rotation);
				go.name = objectList [currentObject].name;

				objectList [currentObject].GetComponent<ObjectMenu> ().currentAvailable--;
				UpdateObjectInfo ();


			}
		}
	}

	public void DeleteSpawnedObject(string objectName){
		foreach (Transform child in transform) {
			if (child.CompareTag ("MenuObject")) {
				if (child.name == objectName) {
					if (child.GetComponent<ObjectMenu> ().available) {
						child.GetComponent<ObjectMenu> ().currentAvailable++;
						if (child.GetComponent<ObjectMenu> ().currentAvailable > child.GetComponent<ObjectMenu> ().maxAvailable)
							child.GetComponent<ObjectMenu> ().currentAvailable = child.GetComponent<ObjectMenu> ().maxAvailable;
						UpdateObjectInfo ();
					}
				}
			}
		}
	}

	public void MenuUp(){
		if (solutionSolid != null)
			solutionSolid.SetActive (!solutionSolid.activeSelf);
	}

	public void MenuDown(){
		if (solution != null)
			solution.SetActive (!solution.activeSelf);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
