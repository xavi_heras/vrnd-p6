﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ControllerInputManager : MonoBehaviour {
	public SteamVR_TrackedObject trackedObj;
	public SteamVR_Controller.Device device;
	public Player steamVRPlayer;
	public Hand rightHand;
	public Hand leftHand;
	public Hand.HandType type;

	//Teleport
	public bool enableTeleport = false;
	public bool canTeleportToStart = false;

	public LineRenderer laser;
	public GameObject teleportAimerObject;
	public Vector3 teleportLocation;
	public GameObject player;
	public LayerMask laserMask;
	public float yNudgeAmount = 1f; //teleportAimerObject height

	 //Throw
	public float throwForce = 1.5f;

	//Structure menu
	public ObjectMenuManager menu;
	private bool menuMode = false;

	public GameObject ball;
	public GameObject start;

	void Start () {
		
		trackedObj = GetComponent<SteamVR_TrackedObject> ();	
		ball = GameObject.Find ("Ball");
		start = GameObject.Find ("Start");
		if (laser)
			laser.gameObject.SetActive (false);
	}
	
	void Update () {
		device = SteamVR_Controller.Input ((int)trackedObj.index);

		if (type == Hand.HandType.Left) {
			if (rightHand.controller !=null)
				if (rightHand.controller.index == device.index)
					SetTeleportHand (rightHand);			
			
			if (leftHand.controller !=null)
				if (leftHand.controller.index == device.index)
					SetTeleportHand (leftHand);			
		}

		if (laser != null && enableTeleport) { //left hand - teleport
			
			if (device.GetPress (SteamVR_Controller.ButtonMask.Touchpad)) {
				laser.gameObject.SetActive (true);
				teleportAimerObject.SetActive (true);


				laser.SetPosition (0, gameObject.transform.position);
				RaycastHit hit;
				if (Physics.Raycast (transform.position, transform.forward, out hit, 15, laserMask)) {
					teleportLocation = hit.point;
					laser.SetPosition (1, teleportLocation);
					//aimer
					teleportAimerObject.transform.position = new Vector3 (teleportLocation.x, teleportLocation.y + yNudgeAmount, teleportLocation.z);
				} else {
					/*
					teleportLocation = transform.forward * 15 + transform.position;
					RaycastHit groundRay;
					if (Physics.Raycast (teleportLocation, -Vector3.up, out groundRay, 17, laserMask)) {
						teleportLocation = groundRay.point;
					}
					laser.SetPosition (1, transform.forward * 15 + transform.position);
					//aimer position
					teleportAimerObject.transform.position = teleportLocation + new Vector3 (0, yNudgeAmount, 0);
					*/
				}
			} else {
				laser.gameObject.SetActive (false);
			}

			if (device.GetPressUp (SteamVR_Controller.ButtonMask.Touchpad)) {
				laser.gameObject.SetActive (false);
				teleportAimerObject.SetActive (false);
				player.transform.position = teleportLocation;
			}
		} 



		if (menu != null) { //right hand
			
			if (device.GetTouch (SteamVR_Controller.ButtonMask.Touchpad)) {
				menu.Show ();
				menuMode = true;

				if (device.GetPressDown (SteamVR_Controller.ButtonMask.Touchpad)) {
					Vector2 touchCoor = device.GetAxis (Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
					float distance = .5f;

					if (touchCoor.x >= distance)
						menu.MenuRight ();
					else if (touchCoor.x <= -distance)
						menu.MenuLeft ();

					if (touchCoor.y >= distance)
						menu.MenuUp ();
					else if (touchCoor.y <= -distance)
						menu.MenuDown ();
					
				}

				if (device.GetPressDown (SteamVR_Controller.ButtonMask.Trigger)) {
					menu.SpawnCurrentObject ();
				}
			} 

			if (device.GetTouchUp (SteamVR_Controller.ButtonMask.Touchpad)) {
				menu.Hide ();	
				menuMode = false;
			}

		} else {  //left hand
			if (canTeleportToStart) {
				if (device.GetPressDown (SteamVR_Controller.ButtonMask.Grip)) {
					player.transform.position = start.transform.position;			
				}
			}
		}


		if (device.GetPressDown (SteamVR_Controller.ButtonMask.ApplicationMenu)) {
			if (ball != null) {
				ball.GetComponent<Ball> ().Reset ();
			}
		}
			
	}

	void SetTeleportHand (Hand teleportHand){
		bool replace = false;
		if (steamVRPlayer.hands.Length == 0) {
			replace = true;
		} else {
			if (steamVRPlayer.hands [0] != teleportHand)
				replace = true;
		}
		if (replace) 			
			steamVRPlayer.hands [0] = teleportHand;
	}

	void OnTriggerStay(Collider col){
		if (col.gameObject.CompareTag("Throwable") || col.gameObject.CompareTag("Structure")){
			if (device != null && !menuMode) {
				if (device.GetPressUp (SteamVR_Controller.ButtonMask.Trigger)) {
					ThrowObject (col);
				} else if (device.GetPressDown (SteamVR_Controller.ButtonMask.Trigger)) {
					GrabObject (col);
				}


				if (menu != null) { //right hand
					//delete structure objects with right grip control
					if (device.GetPressDown (SteamVR_Controller.ButtonMask.Grip)) {
						if (col.gameObject.CompareTag ("Structure")) {
							if (col.gameObject.name.Contains ("Portal")) {
								col.transform.parent.GetComponent<PortalSystem> ().DestroySystem ();
								menu.DeleteSpawnedObject ("Portal");
							} else {
								Destroy (col.gameObject);
								if (menu != null) {
									menu.DeleteSpawnedObject (col.gameObject.name);
								}
							}
						}
					}
				}

			}
		}
	}

	void GrabObject (Collider col){
		col.transform.SetParent (gameObject.transform);
		col.GetComponent<Rigidbody> ().isKinematic = true;
		device.TriggerHapticPulse (2000);

		if (col.name.Contains ("Ball")) {
			ball.GetComponent<Ball> ().playing = false;
		}
	}

	void ThrowObject (Collider col){
		col.transform.SetParent (null);
		if (col.name.Contains ("Portal"))
			col.GetComponent<PortalContainer> ().ResetParent ();

		Rigidbody rigidBody = col.GetComponent<Rigidbody> ();
		rigidBody.isKinematic = false;
		rigidBody.velocity = device.velocity * throwForce;
		rigidBody.angularVelocity = device.angularVelocity;	

		//ball flag to know when it is released
		if (col.name.Contains ("Ball")) {
			ball.GetComponent<Ball> ().playing = true;
			//ball is valid only if released from inside start area
			if (!start.GetComponent<StartPlatform> ().PlayerInside) {
				ball.GetComponent<Ball> ().SetCheatBall ();
			}

		}

	}
}
