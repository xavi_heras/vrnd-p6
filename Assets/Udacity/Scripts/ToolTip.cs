﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTip : MonoBehaviour {

	public string description = "Tooltip text";

	public Canvas canvas;

	public Image image;
	public Vector2 imageSize = new Vector2 (100, 30);
	public Color imageColor;

	public Text text;
	public Color textColor;
	public int textSize;

	public LineRenderer line;
	public Color lineColor;
	public float lineWidth;
	public Transform drawLineFrom;
	public Transform drawLineTo;

	// Use this for initialization
	void Start () {
		ResetTooltip ();
	}
	
	// Update is called once per frame
	void Update () {
		DrawLine ();
	}

	void ResetTooltip(){
		//SetContainer ();
		SetText ();
		SetLine ();
		if (drawLineTo == null && transform.parent != null) {
			drawLineTo = transform.parent;
		}
	}

	void SetContainer(){
		canvas.GetComponent<RectTransform> ().sizeDelta = imageSize;
		image.transform.GetComponent<RectTransform> ().sizeDelta = imageSize;
		image.color = imageColor;
	}

	void SetText(){
		text.text = description.Replace ("\\n", "\n");
		text.color = textColor;
		text.fontSize = textSize;
	}

	void SetLine (){
		//line = transform.Find("Line").GetComponent<LineRenderer>();
		//line.material = Resources.Load("TooltipLine") as Material;
		line.material.color = lineColor;
		#if UNITY_5_5_OR_NEWER
		line.startColor = lineColor;
		line.endColor = lineColor;
		line.startWidth = lineWidth;
		line.endWidth = lineWidth;
		#else
		line.SetColors(lineColor, lineColor);
		line.SetWidth(lineWidth, lineWidth);
		#endif
		if (drawLineFrom == null)
		{
			drawLineFrom = transform;
		}
	}

	void DrawLine(){
		if (drawLineTo != null) {
			line.SetPosition (0, drawLineFrom.position);
			line.SetPosition (1, drawLineTo.position);
		}
	}
}
