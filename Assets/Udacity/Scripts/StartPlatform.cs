﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPlatform : MonoBehaviour {

	public bool  PlayerInside = false;
	public GameObject goal;
	public GameObject ball;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!PlayerInside)
			if (!ball.GetComponent<Ball>().playing)
				ball.GetComponent<Ball> ().SetCheatBall ();
		else
			if (!ball.GetComponent<Ball>().playing)
				ball.GetComponent<Ball> ().SetNormalBall ();
	}

	void OnTriggerStay(Collider col){
		if (col.CompareTag("Player")){	
			PlayerInside = true;
			//goal.GetComponent<Goal> ().SetLightON ();
			if (!ball.GetComponent<Ball>().playing)
				ball.GetComponent<Ball> ().SetNormalBall ();
		}
	}

	void OnTriggerExit(Collider col){
		if (col.CompareTag("Player")){	
			PlayerInside = false;
			//goal.GetComponent<Goal> ().SetLightOFF ();
			if (!ball.GetComponent<Ball>().playing)
				ball.GetComponent<Ball> ().SetCheatBall ();
		}
	}

}
