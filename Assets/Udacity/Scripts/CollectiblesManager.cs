﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectiblesManager : MonoBehaviour {

	public static GameObject[] collectibles;

	// Use this for initialization
	void Start () {
		
		collectibles = GameObject.FindGameObjectsWithTag ("Collectible");

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void ResetCollectibles(){
		foreach (GameObject collectible in collectibles) {
			collectible.SetActive (true);
		}
	}

	public static bool AllCollectiblesCollected(){
		bool ret = true;
		foreach (GameObject collectible in collectibles) {
			if (collectible.activeInHierarchy) {
				ret = false;
				break;
			}
		}
		return ret;
	}

}
