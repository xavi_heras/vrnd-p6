﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviourSingletonPersistent<Game> {

	public string[] levels;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Alpha1))
			SteamVR_LoadLevel.Begin (levels [0], true);
		else if (Input.GetKeyDown (KeyCode.Alpha2))
			SteamVR_LoadLevel.Begin (levels [1], true);
		else if (Input.GetKeyDown (KeyCode.Alpha3))
			SteamVR_LoadLevel.Begin (levels [2], true);
		else if (Input.GetKeyDown (KeyCode.Alpha4))
			SteamVR_LoadLevel.Begin (levels [3], true);
		else if (Input.GetKeyDown (KeyCode.Alpha5))
			SteamVR_LoadLevel.Begin (levels [4], true);
		else if (Input.GetKeyDown (KeyCode.Alpha6))
			SteamVR_LoadLevel.Begin (levels [5], true);
		else if (Input.GetKeyDown (KeyCode.Alpha7))
			SteamVR_LoadLevel.Begin (levels [6], true);
		else if (Input.GetKeyDown (KeyCode.R))
			SteamVR_LoadLevel.Begin (SceneManager.GetActiveScene ().name, true);
		else if (Input.GetKeyDown (KeyCode.Escape))
			Application.Quit ();
		else if (Input.GetKeyDown (KeyCode.I)) { //Fade to black
			SteamVR_Fade.View (Color.clear,0f);
			SteamVR_Fade.View (Color.black ,3f);

		} else if (Input.GetKeyDown (KeyCode.O)) { //Fade from black
			SteamVR_Fade.View (Color.black ,0f);
			SteamVR_Fade.View (Color.clear ,3f);
		}
	
	}
}
