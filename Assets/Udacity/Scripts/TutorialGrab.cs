﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialGrab : MonoBehaviour {

	public GameObject ball;
	private Vector3 ballPosition;

	// Use this for initialization
	void Start () {
		ballPosition = ball.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (ball.transform.position != ballPosition) {
			Destroy (this.gameObject);
		}
	}
}
