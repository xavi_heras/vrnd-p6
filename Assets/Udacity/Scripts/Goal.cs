﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour {
	
	public GameObject lightON;
	public GameObject lightOFF;
	public GameObject ball;

	public string nextLevel = "";

	private AudioSource audio;

	public bool finished = false;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		SetLightNone ();
		ball = GameObject.Find ("Ball");
	}

	public void Reset (){
		SetLightNone ();
		finished = false;
	}

	public void SetLightON(){
		lightON.SetActive (true);
		lightOFF.SetActive (false);
	}

	public void SetLightOFF(){		
		lightON.SetActive (false);
		lightOFF.SetActive (true);
	}

	public void SetLightBoth(){
		lightON.SetActive (true);
		lightOFF.SetActive (true);
	}

	public void SetLightNone(){
		lightON.SetActive (false);
		lightOFF.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider col){
		if (col.gameObject.name == "Ball"){	
			if (CollectiblesManager.AllCollectiblesCollected ()) {
				//Debug.Log ("Next level!");
				if (ball.GetComponent<Ball> ().valid) {
					SetLightBoth ();
					if (!finished) {
						finished = true;
						audio.PlayOneShot (audio.clip);
						if (nextLevel != "")
							StartCoroutine (LoadNextLevel (2f));
					}
				} else {
					SetLightOFF();
					ball.GetComponent<Ball> ().ForceBallReset ();
				}
			} else {
				//Debug.Log ("Collectibles not collected.. wait");
				SetLightOFF();
				ball.GetComponent<Ball> ().ForceBallReset ();
			}
		}
	}


	IEnumerator LoadNextLevel(float time){
		yield return new WaitForSeconds (time);
		SteamVR_LoadLevel.Begin (nextLevel, false, 2f);
	}
}
