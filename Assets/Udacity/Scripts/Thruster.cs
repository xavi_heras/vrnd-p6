﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thruster : MonoBehaviour {
	public float force = 30f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider col){
		if (col.gameObject.name == "Ball"){	
			col.gameObject.GetComponent<Rigidbody> ().AddForce (transform.up * force);
		}
	}
}
